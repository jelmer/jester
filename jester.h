/* jester.h */
/* header file for jester
   Copyright (C) 1998 Matthew Grossman <mattg@oz.net> */
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/



#ifndef _JESTER_H
#define _JESTER_H

/*********************/
/* includes          */

#include<X11/Xlib.h>
#include<X11/Xresource.h>
#include<X11/Xutil.h>
#include<X11/cursorfont.h>

#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<unistd.h>
#include<time.h>

/*********************/
/* defines           */

#define EMPTY 0
#define WHITE 1
#define BLACK 2
#define SQUARE_WIDTH 50
#define BORDER_WIDTH 10
#define BUTTON_HEIGHT 20
#define EXIT_BUTTON_WIDTH 50
#define SCORE_WINDOW_WIDTH 200
#define PLAYER_ID_WINDOW_WIDTH 50
#define BOARD_WIDTH ((SQUARE_WIDTH * 8) + (BORDER_WIDTH * 2))
#define BOARD_HEIGHT ((SQUARE_WIDTH * 8) + (BORDER_WIDTH * 3) + (BUTTON_HEIGHT))


/*********************/
/* globals           */


enum directions { up = 1, up_right, right, down_right, down, down_left,
		    left, up_left };

struct coords {
  int x;
  int y;
};
 
struct jester_data {
  int (*expose_function)();
  int (*buttonpress_function)();
  struct coords coords;
};

Display *display;
int screen_number;
Window board_window;
Window square_windows[8][8];
Window score_window;
Window exit_button_window;
Window player_id_window;
int pieces[8][8];
GC gc;

unsigned long green;
unsigned long black;
unsigned long white;
unsigned long white_piece;
unsigned long black_piece;
unsigned long white_shadow;
unsigned long white_highlight;
unsigned long black_shadow;
unsigned long black_highlight;

XContext jester_context;

int num_black_pieces;
int num_white_pieces;

Font jester_font;

Cursor legal_cursor;

int ai; /* which side, if any, is the computer playing? */

/*********************/
/* functions         */

char *get_display_name(int argc, char *argv[]);
int get_screen_number(char *display_name);
int set_up_stuff(int argc, char *argv[]);
int handle_events();
void set_up_fonts();
void set_up_colors();
void set_up_pieces();
caddr_t proper_function(struct jester_data *jd, XEvent *event);
int square_expose(XEvent *event);
void draw_piece(int player, Window window);
unsigned long color_from_player(int player);
int legal_move(int x, int y, int player);
int square_buttonpress(XEvent *event);
int board_expose(XEvent *event);
struct jester_data *new_jd();
void delete_jd(struct jester_data *jd);
void clean_up_stuff();
int flip_pieces(struct coords coords);
int check_piece(int x, int y, int color);
int flip_line(int x, int y, enum directions direction,
	       int new_color, int testp);
void really_flip_line(int start_x, int start_y, int end_x, int end_y,
		      int new_color);
int other_color(int s, int c);
int count_pieces(int player);
int can_player_move(int player);
int other_player(int player);
int exit_button_expose(XEvent *event);
int exit_button_buttonpress(XEvent *event);
int center_text_x(Display *display, int width, char *text, Font font);
int center_text_y(Display *display, int height, Font font);
int get_font_height(Display *display, Font font);
int get_string_width(Display *display, char *s, Font font);
int score_window_expose(XEvent *event);
int update_score_window(char *format);
int player_id_window_expose(XEvent *event);
void show_player_id();
void show_restart();
int player_id_window_buttonpress(XEvent *event);
void set_up_cursors();
void mark_legal_squares(int player);
void create_board_window(int argc, char *argv[]);
void create_exit_button();
void create_score_window();
void create_player_id_window();
void create_square_windows();
char *get_text_option(int argc, char *argv[], char *option);
void make_ai_move();
void get_best_ai_move(struct coords *move);
int get_bool_option(int argc, char *argv[], char *option);
void print_usage();
int get_us_position(int argc, char *argv[], int *x, int *y);




#endif /* !_JESTER_H */
